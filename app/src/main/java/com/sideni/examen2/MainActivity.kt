package com.sideni.examen2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AlertDialog

class MainActivity : AppCompatActivity() {


    lateinit var btnLogin: Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnLogin = findViewById(R.id.boton)
        btnLogin.setOnClickListener({

                showAlert()
        })

    }

    fun showAlert(){
        var alertDialog = AlertDialog.Builder(this)
        alertDialog.setTitle("Aviso")
        alertDialog.setMessage("el Tiempo ha terminado")
        alertDialog.setNegativeButton("Reintentar", null)
        alertDialog.show()
    }
}
